import React from 'react'

export default function Time(props) {
    return (
        <p className='xs'>
            {props.date.toLocaleTimeString()}
        </p>
    )
}

